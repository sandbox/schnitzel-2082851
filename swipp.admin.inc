<?php

/**
 * @file
 * Admin functions for swipp.
 */

/**
 * Settings for the static FB Like button block.
 */
function swipp_admin_settings_form() {
  global $base_url;
  include 'swipp.php';

  $false = TRUE;
  if (variable_get('swipp_account_email', FALSE)) {
    // Try to login to tell the user if the connection works.
    $response = swipp_signin(variable_get('swipp_account_email', ''), variable_get('swipp_account_password', ''));

    if ($response['status'] == '200') {
      drupal_set_message(t('Successfully connected to Swipp API.'));
      $connected = TRUE;
    }
  }


  $form['swipp'] = array(
    '#type' => 'fieldset',
    '#title' => t('Swipp API Connection'),
    '#collapsible' => FALSE,
  );

  $form['swipp']['swipp_account_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#default_value' => variable_get('swipp_account_email', ''),
  );
  $form['swipp']['swipp_account_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('swipp_account_password', ''),
  );
  $form['swipp']['swipp_account_firstname'] = array(
    '#type' => 'textfield',
    '#title' => t('Firstname'),
    '#default_value' => variable_get('swipp_account_firstname', ''),
  );
  $form['swipp']['swipp_account_lastname'] = array(
    '#type' => 'textfield',
    '#title' => t('Lastname'),
    '#default_value' => variable_get('swipp_account_lastname', ''),
  );

  $form['#validate'] = array('swipp_admin_settings_form_validate');
  return system_settings_form($form);
}

/**
 * Validation handler for fblikebutton_dynamic_settings form.
 */
function swipp_admin_settings_form_validate($form, &$form_state) {

  include 'swipp.php';

  // First try to register the User.
  $response = swipp_signup($form_state['values']['swipp_account_email'], $form_state['values']['swipp_account_password'], $form_state['values']['swipp_account_firstname'], $form_state['values']['swipp_account_lastname']);

  // Response 200 means User created.
  // 403 with Error Code ALREADY_SIGNED_UP_ERROR is Account exists allready,
  // in both cases we just continue.
  if ($response['status'] != '200' && !($response['status'] == '403' && $response['response']->errorInfo->errorCode == 'ALREADY_SIGNED_UP_ERROR')) {
    drupal_set_message(t('Swipp API Signup Error: @errorCode, @errorMessage', array('@errorCode' => $response['response']->errorInfo->errorCode, '@errorMessage' => isset($response['response']->errorInfo->errorMessage) ? $response['response']->errorInfo->errorMessage : '')), 'error');
    variable_set('swipp_account_userguid', FALSE);
    variable_set('swipp_account_accesstoken', FALSE);
    return;
  }

  // Now try to login.
  $response = swipp_signin($form_state['values']['swipp_account_email'], $form_state['values']['swipp_account_password']);
  if ($response['status'] != '200') {
    drupal_set_message(t('Swipp API Signin Error: @errorCode, @errorMessage', array('@errorCode' => $response['response']->errorInfo->errorCode, '@errorMessage' => isset($response['response']->errorInfo->errorMessage) ? $response['response']->errorInfo->errorMessage : '')), 'error');
    variable_set('swipp_account_userguid', FALSE);
    variable_set('swipp_account_accesstoken', FALSE);
    return;
  }
  else {
    variable_set('swipp_account_userguid', $response['response']->signInOutput->userGuid);
    variable_set('swipp_account_accesstoken', $response['response']->signInOutput->accessToken);
  }

  // Now get Organisation Name.
  $response = swipp_get_organisation(variable_get('swipp_account_userguid'), variable_get('swipp_account_accesstoken'));

  if ($response['status'] != '200') {
    drupal_set_message(t('Swipp API Organisation get Error: @errorCode, @errorMessage', array('@errorCode' => $response['response']->errorInfo->errorCode, '@errorMessage' => isset($response['response']->errorInfo->errorMessage) ? $response['response']->errorInfo->errorMessage : '')), 'error');
    return;
  }
  else {
    variable_set('swipp_account_organisationid', $response['response']->orgAccountDetails[0]->id);
  }


}
