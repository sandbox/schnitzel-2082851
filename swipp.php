<?php

/**
 * @file
 * Function calls to the Swipp API.
 */

/**
 * Creates a new Account, returns 200 if succeded or 403 if already existing.
 */
function swipp_signup($email, $password, $firstname, $lastname) {
  $uri    = SWIPP_API_URL . "/user/usersignup?appId=" . SWIPP_APP_ID . "&appToken=" . SWIPP_APP_TOKEN;
  $date   = gmdate(DATE_RFC822);
  $header = array("Date: $date", "Content-Type: application/json");

  $body = json_encode(array(
    "accountType" => 1,
    "emailAddress" => $email,
    "firstName" => $firstname,
    "lastName" => $lastname,
    "accountToken" => base64_encode($password),
  ));

  $ret = curlRequest($uri, $header, "POST", $body);

  return $ret;
}

/**
 * Tries to login the User.
 */
function swipp_signin($email, $password) {
  $uri      = SWIPP_API_URL . "/user/usersignin?appId=" . SWIPP_APP_ID . "&appToken=" . SWIPP_APP_TOKEN;
  $date     = gmdate(DATE_RFC822);
  $header   = array("Date: $date", "Content-Type: application/json");

  $body = json_encode(array(
    "accountType" => 1,
    "emailAddress"  => $email,
    "accountToken" => base64_encode($password),
  ));


  $ret = curlRequest($uri, $header, "PUT", $body);
  return $ret;
}

/**
 * Gets the Organisation ID and sets the Organisation Name if not yet set.
 */
function swipp_get_organisation($user_guid, $access_token) {

  $user_guid      = base64_encode($user_guid);
  $access_token   = base64_encode($access_token);

  $uri    = SWIPP_API_URL . "/widget/orgaccount?userGuid=" . $user_guid . "&accessToken=" . $access_token;
  $date   = gmdate(DATE_RFC822);
  $header = array("Date: $date", "Content-Type: application/json");

  $ret = curlRequest($uri, $header, 'GET', NULL);

  if (count($ret['response']->orgAccountDetails) <= 0) {
    $body = json_encode(array("companyName" => variable_get('site_name', "Default site name")));
    $ret = curlRequest($uri, $header, 'POST', $body);
  }

  return $ret;
}

/**
 * Helper function which returns widget keys for both widget types.
 */
function swipp_get_both_widget_keys($term) {
  $return = array();

  // We save both keys in a array keyed by the type.
  $return[1] = swipp_get_widget_key($term, 1);
  $return[3] = swipp_get_widget_key($term, 3);

  return $return;

}

/**
 * Queries the API to get the Term ID and Widget Key.
 */
function swipp_get_widget_key($term, $type = 1) {
  $payload = array(
    'term' => $term,
    'type' => $type,
  );

  $org_id         = variable_get('swipp_account_organisationid', '');
  $user_guid      = base64_encode(variable_get('swipp_account_userguid', ''));
  $access_token   = base64_encode(variable_get('swipp_account_accesstoken', ''));

  $uri    = SWIPP_API_URL . "/widget/orgaccount/$org_id/orguser/widget?userGuid=$user_guid&accessToken=$access_token";

  $date   = gmdate(DATE_RFC822);
  $header = array("Date: $date", "Content-Type: application/json");

  $ret = curlRequest($uri, $header, 'POST', json_encode($payload));

  return $ret;
}

/**
 * Shared function quering the API.
 */
function curlRequest($uri, $header, $method, $body) {

  $ch = curl_init($uri);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
  if ($body !== NULL) {
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    $ret = array(
      'response'  => json_decode(curl_exec($ch)),
      'status'    => curl_getinfo($ch, CURLINFO_HTTP_CODE),
    );
  }
  else {
    $ret = array(
      'response'  => json_decode(curl_exec($ch)),
      'status'    => curl_getinfo($ch, CURLINFO_HTTP_CODE),
    );
  }
  return $ret;
}
